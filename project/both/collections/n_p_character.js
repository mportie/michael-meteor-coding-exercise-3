NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  healthPoints:{
    type:Number
  },
  charmed:{
    type:Boolean
  },
  
  npCharacterRequestIds:{
    type:[String],
    optional: true
  }

}));
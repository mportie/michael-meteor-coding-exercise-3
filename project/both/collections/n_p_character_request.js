NPCharacterRequest = new Mongo.Collection('n_p_character_request');

NPCharacterRequest.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
   Speech:{
    type:[String]
  },
  npCharacterResponseIds:{
    type:[String]
  }

}));